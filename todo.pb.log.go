package todo

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	activityLog "go.saastack.io/activity-log"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/todo/pb"
	"go.uber.org/zap"
)

type logsTodosServer struct {
	pb.TodosServer
	actLogCli activityLogPb.ActivityLogsClient
}

func NewLogsTodosServer(
	actLogCli activityLogPb.ActivityLogsClient,
	s pb.TodosServer,
) pb.TodosServer {
	srv := &logsTodosServer{s, actLogCli}
	return srv
}

func (s *logsTodosServer) CreateTodo(ctx context.Context, in *pb.CreateTodoRequest) (*pb.Todo, error) {

	res, err := s.TodosServer.CreateTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.CreateTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.CreateTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) GetTodo(ctx context.Context, in *pb.GetTodoRequest) (*pb.Todo, error) {

	res, err := s.TodosServer.GetTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.GetTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.GetTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) DeleteTodo(ctx context.Context, in *pb.DeleteTodoRequest) (*empty.Empty, error) {

	res, err := s.TodosServer.DeleteTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.DeleteTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.DeleteTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) UpdateTodo(ctx context.Context, in *pb.UpdateTodoRequest) (*pb.Todo, error) {

	res, err := s.TodosServer.UpdateTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.UpdateTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.UpdateTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) ListTodo(ctx context.Context, in *pb.ListTodoRequest) (*pb.ListTodoResponse, error) {

	res, err := s.TodosServer.ListTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.ListTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.ListTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) BatchGetTodo(ctx context.Context, in *pb.BatchGetTodoRequest) (*pb.BatchGetTodoResponse, error) {

	res, err := s.TodosServer.BatchGetTodo(ctx, in)
	if err != nil {
		return nil, err
	}

	//TODO: Fill parent
	parent := ""

	//TODO: Fill this according to the needs
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          parent,
		Data:            nil,
		EventName:       ".saastack.v1.Todos.BatchGetTodo", // event
		ActivityId:      "",
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		//TODO: Fill ID
		ctxzap.Extract(ctx).Error("Unable to create activity log", zap.Error(err), zap.String("EventName", ".saastack.v1.Todos.BatchGetTodo"), zap.String("Id", ""))
		return nil, err
	}

	return res, nil
}

func (s *logsTodosServer) ListFilterToDo(ctx context.Context, in *pb.ListFilterToDoRequest) (*pb.ListTodoResponse, error) {

	res, err := s.TodosServer.ListFilterToDo(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil
}
