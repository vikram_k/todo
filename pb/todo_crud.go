package pb

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	"go.saastack.io/protos/types"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TodosServiceTodoServerCrud struct {
	store TodoStore
	bloc  TodosServiceTodoServerBLoC
}

type TodosServiceTodoServerBLoC interface {
	GetTodoBLoC(context.Context, *GetTodoRequest) error

	UpdateTodoBLoC(context.Context, *UpdateTodoRequest) error

	DeleteTodoBLoC(context.Context, *DeleteTodoRequest) error

	BatchGetTodoBLoC(context.Context, *BatchGetTodoRequest) error

	ListTodoBLoC(context.Context, *ListTodoRequest) (TodoCondition, error)
}

func NewTodosServiceTodoServerCrud(s TodoStore, b TodosServiceTodoServerBLoC) *TodosServiceTodoServerCrud {
	return &TodosServiceTodoServerCrud{store: s, bloc: b}
}

func (s *TodosServiceTodoServerCrud) UpdateTodo(ctx context.Context, in *UpdateTodoRequest) (*Todo, error) {

	mask := s.GetViewMask(in.UpdateMask)
	if len(mask) == 0 {
		return nil, status.Error(codes.InvalidArgument, "cannot send empty update mask")
	}

	if err := in.GetTodo().Validate(mask...); err != nil {
		return nil, err
	}

	err := s.bloc.UpdateTodoBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.UpdateTodo(ctx,
		in.Todo, mask,
		TodoIdEq{Id: in.Todo.Id},
	); err != nil {
		return nil, err
	}

	updatedTodo, err := s.store.GetTodo(ctx, []string{},
		TodoIdEq{
			Id: in.GetTodo().GetId(),
		},
	)
	if err != nil {
		return nil, err
	}

	return updatedTodo, nil
}

func (s *TodosServiceTodoServerCrud) GetTodo(ctx context.Context, in *GetTodoRequest) (*Todo, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.GetTodoBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	res, err := s.store.GetTodo(ctx, mask, TodoIdEq{Id: in.Id})
	if err != nil {
		if err == errors.ErrNotFound {
			return nil, status.Error(codes.NotFound, "Todo not found")
		}
		return nil, err
	}

	return res, nil
}

func (s *TodosServiceTodoServerCrud) ListTodo(ctx context.Context, in *ListTodoRequest) (*ListTodoResponse, error) {

	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	condition, err := s.bloc.ListTodoBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	page, err := s.GetPagedCondition(ctx, in.First, in.After, in.Last, in.Before)
	if err != nil {
		return nil, err
	}

	mask := s.GetViewMask(in.ViewMask)

	return s.ListWithPagination(ctx, page, condition, mask)
}

func (s *TodosServiceTodoServerCrud) ListWithPagination(ctx context.Context, page *CursorBasedPagination, condition TodoCondition, viewMask []string) (*ListTodoResponse, error) {

	list, err := s.store.ListTodos(ctx,
		viewMask,
		condition,
		page,
	)
	if err != nil {
		return nil, err
	}

	res := &ListTodoResponse{
		PageInfo: &types.PageInfo{},
	}

	for _, it := range list {

		res.Nodes = append(res.Nodes, &TodoNode{Position: it.Id, Node: it})
	}
	res.PageInfo.HasPrevious = page.HasPrevious
	res.PageInfo.HasNext = page.HasNext
	if len(list) > 0 {
		res.PageInfo.StartCursor = list[0].Id
		res.PageInfo.EndCursor = list[len(list)-1].Id
	}

	return res, nil

}

func (s *TodosServiceTodoServerCrud) GetPagedCondition(ctx context.Context, first uint32, after string, last uint32, before string) (*CursorBasedPagination, error) {

	page := &CursorBasedPagination{}
	flag := false

	if first != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(after),
			Limit:    int(first),
			UpOrDown: false,
		}

	} else if last != 0 {
		flag = true
		page = &CursorBasedPagination{
			Cursor:   idutil.GetId(before),
			Limit:    int(last),
			UpOrDown: true,
		}
	}
	if !flag {
		return nil, status.Error(codes.InvalidArgument, "either after-first or before-last should be set in request")
	}

	return page, nil
}

func (s *TodosServiceTodoServerCrud) DeleteTodo(ctx context.Context, in *DeleteTodoRequest) (*empty.Empty, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.DeleteTodoBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := s.store.DeleteTodo(ctx, TodoIdEq{Id: in.Id}); err != nil {
		return nil, err
	}

	return &empty.Empty{}, nil
}

func (s *TodosServiceTodoServerCrud) BatchGetTodo(ctx context.Context, in *BatchGetTodoRequest) (*BatchGetTodoResponse, error) {
	if err := in.Validate(); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	err := s.bloc.BatchGetTodoBLoC(ctx, in)
	if err != nil {
		return nil, err
	}

	getIds := make([]string, 0, len(in.Ids))
	for _, id := range in.Ids {
		getIds = append(getIds, idutil.GetId(id))
	}

	mask := s.GetViewMask(in.ViewMask)

	list, err := s.store.ListTodos(ctx, mask, TodoIdIn{Id: getIds})
	if err != nil {
		return nil, err
	}

	resultMap := make(map[string]*Todo, 0)
	for i, it := range list {
		_ = i

		resultMap[it.Id] = it
	}

	isGrpc := userinfo.IsGrpcCall(ctx)

	result := make([]*Todo, 0, len(in.Ids))
	for _, id := range in.Ids {
		if resultMap[id] == nil && isGrpc {
			result = append(result, &Todo{})
			continue
		}
		result = append(result, resultMap[id])
	}

	return &BatchGetTodoResponse{Todo: result}, nil
}

func (s *TodosServiceTodoServerCrud) GetViewMask(mask *field_mask.FieldMask) []string {
	if mask == nil || mask.GetPaths() == nil {
		return []string{}
	}
	return mask.GetPaths()
}
