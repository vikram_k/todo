package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	any "github.com/golang/protobuf/ptypes/any"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
	types "go.saastack.io/protos/types"
)

var objectTableMap = chaku_globals.ObjectTable{
	"todo": {
		"id":                  "todo",
		"i_2":                 "todo",
		"i_3":                 "todo",
		"i_4":                 "todo",
		"i_5":                 "todo",
		"i_6":                 "todo",
		"i_7":                 "todo",
		"active":              "todo",
		"i_10":                "todo",
		"i_11":                "todo",
		"i_12":                "todo",
		"i_13":                "todo",
		"i_14":                "todo",
		"todo":                "todo",
		"notification_type":   "todo",
		"notification_type_2": "todo",
		"profile_image":       "todo",
		"todo_notes":          "to_do_notes",
		"total_amount":        "todo",
		"address":             "todo",
		"date_range":          "todo",
	},
	"gallery_item": {
		"thumb_image": "todo",
		"large_image": "todo",
	},
	"to_do_notes": {
		"id":           "to_do_notes",
		"hard_copy":    "to_do_notes",
		"digital_copy": "to_do_notes",
		"details":      "to_do_notes",
	},
	"price": {
		"amount":          "todo",
		"currency":        "todo",
		"amount_in_float": "todo",
	},
	"address": {
		"country":        "todo",
		"locality":       "todo",
		"region":         "todo",
		"postal_code":    "todo",
		"street_address": "todo",
		"latitude":       "todo",
		"longitude":      "todo",
	},
	"dateslot": {
		"start_time": "todo",
		"end_time":   "todo",
	},
}

func (m *Todo) PackageName() string {
	return "saastack_v1"
}

func (m *Todo) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Todo) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	case "profile_image":
		if m.ProfileImage == nil {
			m.ProfileImage = &types.GalleryItem{}
		}
		return m.ProfileImage, nil
	case "todo_notes":
		if len(m.TodoNotes) == 0 {
			m.TodoNotes = append(m.TodoNotes, &ToDoNotes{})
		}
		return m.TodoNotes[0], nil
	case "total_amount":
		if m.TotalAmount == nil {
			m.TotalAmount = &types.Price{}
		}
		return m.TotalAmount, nil
	case "address":
		if m.Address == nil {
			m.Address = &types.Address{}
		}
		return m.Address, nil
	case "date_range":
		if m.DateRange == nil {
			m.DateRange = &types.Dateslot{}
		}
		return m.DateRange, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Todo) ObjectName() string {
	return "todo"
}

func (m *Todo) Fields() []string {
	return []string{
		"id", "i_2", "i_3", "i_4", "i_5", "i_6", "i_7", "active", "i_10", "i_11", "i_12", "i_13", "i_14", "todo", "notification_type", "notification_type_2", "profile_image", "todo_notes", "total_amount", "address", "date_range",
	}
}

func (m *Todo) IsObject(field string) bool {
	switch field {
	case "profile_image", "todo_notes", "total_amount", "address", "date_range":
		return true
	default:
		return false
	}
}

func (m *Todo) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "todo_notes":
		sli := make([]driver.Descriptor, 0, len(m.TodoNotes))
		for _, c := range m.TodoNotes {
			sli = append(sli, c)
		}
		return sli, nil
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Todo) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "i_2":
		return m.I2, nil
	case "i_3":
		return m.I3, nil
	case "i_4":
		return m.I4, nil
	case "i_5":
		return m.I5, nil
	case "i_6":
		return m.I6, nil
	case "i_7":
		return m.I7, nil
	case "active":
		return m.Active, nil
	case "i_10":
		return m.I10, nil
	case "i_11":
		return m.I11, nil
	case "i_12":
		return m.I12, nil
	case "i_13":
		return m.I13, nil
	case "i_14":
		return m.I14, nil
	case "todo":
		return m.Todo, nil
	case "notification_type":
		return m.NotificationType, nil
	case "notification_type_2":
		return m.NotificationType2, nil
	case "profile_image":
		return m.ProfileImage, nil
	case "todo_notes":
		return m.TodoNotes, nil
	case "total_amount":
		return m.TotalAmount, nil
	case "address":
		return m.Address, nil
	case "date_range":
		return m.DateRange, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Todo) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "i_2":
		return &m.I2, nil
	case "i_3":
		return &m.I3, nil
	case "i_4":
		return &m.I4, nil
	case "i_5":
		return &m.I5, nil
	case "i_6":
		return &m.I6, nil
	case "i_7":
		return &m.I7, nil
	case "active":
		return &m.Active, nil
	case "i_10":
		return &m.I10, nil
	case "i_11":
		return &m.I11, nil
	case "i_12":
		return &m.I12, nil
	case "i_13":
		return &m.I13, nil
	case "i_14":
		return &m.I14, nil
	case "todo":
		return &m.Todo, nil
	case "notification_type":
		return &m.NotificationType, nil
	case "notification_type_2":
		return &m.NotificationType2, nil
	case "profile_image":
		return &m.ProfileImage, nil
	case "todo_notes":
		return &m.TodoNotes, nil
	case "total_amount":
		return &m.TotalAmount, nil
	case "address":
		return &m.Address, nil
	case "date_range":
		return &m.DateRange, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Todo) New(field string) error {
	switch field {
	case "id":
		return nil
	case "i_2":
		return nil
	case "i_3":
		return nil
	case "i_4":
		return nil
	case "i_5":
		return nil
	case "i_6":
		return nil
	case "i_7":
		return nil
	case "active":
		return nil
	case "i_10":
		return nil
	case "i_11":
		return nil
	case "i_12":
		return nil
	case "i_13":
		return nil
	case "i_14":
		return nil
	case "todo":
		if m.Todo == nil {
			m.Todo = make([]string, 0)
		}
		return nil
	case "notification_type":
		return nil
	case "notification_type_2":
		return nil
	case "profile_image":
		if m.ProfileImage == nil {
			m.ProfileImage = &types.GalleryItem{}
		}
		return nil
	case "todo_notes":
		if m.TodoNotes == nil {
			m.TodoNotes = make([]*ToDoNotes, 0)
		}
		return nil
	case "total_amount":
		if m.TotalAmount == nil {
			m.TotalAmount = &types.Price{}
		}
		return nil
	case "address":
		if m.Address == nil {
			m.Address = &types.Address{}
		}
		return nil
	case "date_range":
		if m.DateRange == nil {
			m.DateRange = &types.Dateslot{}
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Todo) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "i_2":
		return "int32"
	case "i_3":
		return "int64"
	case "i_4":
		return "int32"
	case "i_5":
		return "int64"
	case "i_6":
		return "int32"
	case "i_7":
		return "int64"
	case "active":
		return "bool"
	case "i_10":
		return "float64"
	case "i_11":
		return "float32"
	case "i_12":
		return "uint32"
	case "i_13":
		return "uint64"
	case "i_14":
		return "uint32"
	case "todo":
		return "repeated"
	case "notification_type":
		return "enum"
	case "notification_type_2":
		return "enum"
	case "profile_image":
		return "message"
	case "todo_notes":
		return "repeated"
	case "total_amount":
		return "message"
	case "address":
		return "message"
	case "date_range":
		return "message"
	default:
		return ""
	}
}

func (_ *Todo) GetEmptyObject() (m *Todo) {
	m = &Todo{}
	_ = m.New("profileImage")
	m.ProfileImage.GetEmptyObject()
	_ = m.New("todoNotes")
	m.TodoNotes = append(m.TodoNotes, &ToDoNotes{})
	m.TodoNotes[0].GetEmptyObject()
	_ = m.New("totalAmount")
	m.TotalAmount.GetEmptyObject()
	_ = m.New("address")
	m.Address.GetEmptyObject()
	_ = m.New("dateRange")
	m.DateRange.GetEmptyObject()
	return
}

func (m *Todo) GetPrefix() string {
	return "tod"
}

func (m *Todo) GetID() string {
	return m.Id
}

func (m *Todo) SetID(id string) {
	m.Id = id
}

func (m *Todo) IsRoot() bool {
	return true
}

func (m *Todo) IsFlatObject(f string) bool {
	switch f {
	case "profile_image", "total_amount", "address", "date_range":
		return true
	default:
		return false
	}
}

func (m *ToDoNotes) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ToDoNotes) ObjectName() string {
	return "to_do_notes"
}

func (m *ToDoNotes) Fields() []string {
	return []string{
		"id", "hard_copy", "digital_copy", "details",
	}
}

func (m *ToDoNotes) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *ToDoNotes) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *ToDoNotes) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "hard_copy":
		return m.HardCopy, nil
	case "digital_copy":
		return m.DigitalCopy, nil
	case "details":
		return m.Details, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ToDoNotes) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "hard_copy":
		return &m.HardCopy, nil
	case "digital_copy":
		return &m.DigitalCopy, nil
	case "details":
		return &m.Details, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *ToDoNotes) New(field string) error {
	switch field {
	case "id":
		return nil
	case "hard_copy":
		return nil
	case "digital_copy":
		if m.DigitalCopy == nil {
			m.DigitalCopy = make([]string, 0)
		}
		return nil
	case "details":
		if m.Details == nil {
			m.Details = make([]*any.Any, 0)
		}
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *ToDoNotes) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "hard_copy":
		return "string"
	case "digital_copy":
		return "repeated"
	case "details":
		return "json"
	default:
		return ""
	}
}

func (_ *ToDoNotes) GetEmptyObject() (m *ToDoNotes) {
	m = &ToDoNotes{}
	return
}

func (m *ToDoNotes) GetPrefix() string {
	return ""
}

func (m *ToDoNotes) GetID() string {
	return m.Id
}

func (m *ToDoNotes) SetID(id string) {
	m.Id = id
}

func (m *ToDoNotes) IsRoot() bool {
	return false
}

func (m *ToDoNotes) IsFlatObject(f string) bool {
	return false
}

func (m *Todo) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	case "to_do_notes":
		return 1
	}
	return 0
}

type TodoStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s TodoStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s TodoStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewTodoStore(d driver.Driver) TodoStore {
	return TodoStore{d: d, limitMultiplier: 1}
}

func NewPostgresTodoStore(db *x.DB, usr driver.IUserInfo) TodoStore {
	return TodoStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type TodoTx struct {
	TodoStore
}

func (s TodoStore) BeginTx(ctx context.Context) (*TodoTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &TodoTx{
		TodoStore: TodoStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *TodoTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *TodoTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s TodoStore) CreateTodoPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_v1;
CREATE TABLE IF NOT EXISTS saastack_v1.todo ( id text DEFAULT ''::text, i_2 integer DEFAULT 0, i_3 integer DEFAULT 0, i_4 integer DEFAULT 0, i_5 integer DEFAULT 0, i_6 integer DEFAULT 0, i_7 integer DEFAULT 0, active boolean DEFAULT false, i_10 real DEFAULT 0.0, i_11 real DEFAULT 0.0, i_12 integer DEFAULT 0, i_13 integer DEFAULT 0, i_14 integer DEFAULT 0, todo text[] DEFAULT '{}'::text[], notification_type integer DEFAULT 0, notification_type_2 integer DEFAULT 0, profile_image_thumb_image text DEFAULT ''::text, profile_image_large_image text DEFAULT ''::text, total_amount_amount integer DEFAULT 0, total_amount_currency text DEFAULT ''::text, total_amount_amount_in_float real DEFAULT 0.0, address_country text DEFAULT ''::text, address_locality text DEFAULT ''::text, address_region text DEFAULT ''::text, address_postal_code text DEFAULT ''::text, address_street_address text DEFAULT ''::text, address_latitude real DEFAULT 0.0, address_longitude real DEFAULT 0.0, date_range_start_time timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, date_range_end_time timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, parent text DEFAULT ''::text, is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text,  PRIMARY KEY ( id, parent ) );
CREATE TABLE IF NOT EXISTS saastack_v1.to_do_notes ( id text DEFAULT ''::text, hard_copy text DEFAULT ''::text, digital_copy text[] DEFAULT '{}'::text[], details jsonb DEFAULT '[]'::jsonb, p0id text DEFAULT ''::text, is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text,  PRIMARY KEY ( id, p0id ) );
CREATE TABLE IF NOT EXISTS saastack_v1.todo_parent ( id text DEFAULT ''::text, parent text DEFAULT ''::text  );
CREATE INDEX IF NOT EXISTS todo_id_index ON saastack_v1.todo_parent ( id );
CREATE INDEX IF NOT EXISTS todo_parent_index ON saastack_v1.todo_parent ( parent );
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s TodoStore) CreateTodos(ctx context.Context, list ...*Todo) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Todo{}, &Todo{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Todo{}, &Todo{}, "", []string{})
}

func (s TodoStore) DeleteTodo(ctx context.Context, cond TodoCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{})
	}
	return s.d.Delete(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{})
}

func (s TodoStore) HardDeleteTodo(ctx context.Context, cond TodoCondition) error {
	if s.withTx {
		return s.tx.HardDelete(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{})
	}
	return s.d.HardDelete(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{})
}

func (s TodoStore) UpdateTodo(ctx context.Context, req *Todo, fields []string, cond TodoCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.todoCondToDriverTodoCond(s.d), req, &Todo{}, fields...)
	}
	return s.d.Update(ctx, cond.todoCondToDriverTodoCond(s.d), req, &Todo{}, fields...)
}

func (s TodoStore) UpdateTodoMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Todo{}, &Todo{}, list...)
}

func (s TodoStore) GetTodo(ctx context.Context, fields []string, cond TodoCondition, opt ...getTodosOption) (*Todo, error) {
	if len(fields) == 0 {
		fields = (&Todo{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listTodosOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListTodos(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TodoStore) ListTodos(ctx context.Context, fields []string, cond TodoCondition, opt ...listTodosOption) ([]*Todo, error) {
	if len(fields) == 0 {
		fields = (&Todo{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetTodoCondition == nil {
					page.SetTodoCondition = defaultSetTodoCondition
				}
				cond = page.SetTodoCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Todo, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Todo{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperTodo(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TodoStore) CountTodos(ctx context.Context, cond TodoCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.todoCondToDriverTodoCond(s.d), &Todo{}, &Todo{})
}

type getTodosOption interface {
	getOptTodos() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptTodos() { // method of no significant use
}

type listTodosOption interface {
	listOptTodos() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptTodos() {
}

func (OrderBy) listOptTodos() {
}

func (*CursorBasedPagination) listOptTodos() {
}

func defaultSetTodoCondition(upOrDown bool, cursor string, cond TodoCondition) TodoCondition {
	if upOrDown {
		if cursor != "" {
			return TodoAnd{cond, TodoIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return TodoAnd{cond, TodoIdGt{cursor}}
	}
	return cond
}

type TodoAnd []TodoCondition

func (p TodoAnd) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.todoCondToDriverTodoCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type TodoOr []TodoCondition

func (p TodoOr) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.todoCondToDriverTodoCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type TodoParentEq struct {
	Parent string
}

func (c TodoParentEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentEq struct {
	Parent string
}

func (c TodoFullParentEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoParentNotEq struct {
	Parent string
}

func (c TodoParentNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentNotEq struct {
	Parent string
}

func (c TodoFullParentNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoParentLike struct {
	Parent string
}

func (c TodoParentLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentLike struct {
	Parent string
}

func (c TodoFullParentLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoParentILike struct {
	Parent string
}

func (c TodoParentILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentILike struct {
	Parent string
}

func (c TodoFullParentILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoParentIn struct {
	Parent []string
}

func (c TodoParentIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentIn struct {
	Parent []string
}

func (c TodoFullParentIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoParentNotIn struct {
	Parent []string
}

func (c TodoParentNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoFullParentNotIn struct {
	Parent []string
}

func (c TodoFullParentNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdEq struct {
	Id string
}

func (c TodoIdEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2Eq struct {
	I2 int32
}

func (c TodoI2Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3Eq struct {
	I3 int64
}

func (c TodoI3Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4Eq struct {
	I4 int32
}

func (c TodoI4Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5Eq struct {
	I5 int64
}

func (c TodoI5Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6Eq struct {
	I6 int32
}

func (c TodoI6Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7Eq struct {
	I7 int64
}

func (c TodoI7Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveEq struct {
	Active bool
}

func (c TodoActiveEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10Eq struct {
	I10 float64
}

func (c TodoI10Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11Eq struct {
	I11 float32
}

func (c TodoI11Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12Eq struct {
	I12 uint32
}

func (c TodoI12Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13Eq struct {
	I13 uint64
}

func (c TodoI13Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14Eq struct {
	I14 uint32
}

func (c TodoI14Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoEq struct {
	Todo []string
}

func (c TodoTodoEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeEq struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2Eq struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2Eq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageEq struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageEq struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdEq struct {
	Id string
}

func (c TodoToDoNotesIdEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyEq struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyEq struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountEq struct {
	Amount uint64
}

func (c TodoPriceAmountEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyEq struct {
	Currency string
}

func (c TodoPriceCurrencyEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatEq struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryEq struct {
	Country string
}

func (c TodoAddressCountryEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityEq struct {
	Locality string
}

func (c TodoAddressLocalityEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionEq struct {
	Region string
}

func (c TodoAddressRegionEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeEq struct {
	PostalCode string
}

func (c TodoAddressPostalCodeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressEq struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeEq struct {
	Latitude float64
}

func (c TodoAddressLatitudeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeEq struct {
	Longitude float64
}

func (c TodoAddressLongitudeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeEq struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeEq struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdNotEq struct {
	Id string
}

func (c TodoIdNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2NotEq struct {
	I2 int32
}

func (c TodoI2NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3NotEq struct {
	I3 int64
}

func (c TodoI3NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4NotEq struct {
	I4 int32
}

func (c TodoI4NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5NotEq struct {
	I5 int64
}

func (c TodoI5NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6NotEq struct {
	I6 int32
}

func (c TodoI6NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7NotEq struct {
	I7 int64
}

func (c TodoI7NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveNotEq struct {
	Active bool
}

func (c TodoActiveNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10NotEq struct {
	I10 float64
}

func (c TodoI10NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11NotEq struct {
	I11 float32
}

func (c TodoI11NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12NotEq struct {
	I12 uint32
}

func (c TodoI12NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13NotEq struct {
	I13 uint64
}

func (c TodoI13NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14NotEq struct {
	I14 uint32
}

func (c TodoI14NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoNotEq struct {
	Todo []string
}

func (c TodoTodoNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeNotEq struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2NotEq struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2NotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageNotEq struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageNotEq struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdNotEq struct {
	Id string
}

func (c TodoToDoNotesIdNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyNotEq struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyNotEq struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountNotEq struct {
	Amount uint64
}

func (c TodoPriceAmountNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyNotEq struct {
	Currency string
}

func (c TodoPriceCurrencyNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatNotEq struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryNotEq struct {
	Country string
}

func (c TodoAddressCountryNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityNotEq struct {
	Locality string
}

func (c TodoAddressLocalityNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionNotEq struct {
	Region string
}

func (c TodoAddressRegionNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeNotEq struct {
	PostalCode string
}

func (c TodoAddressPostalCodeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressNotEq struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeNotEq struct {
	Latitude float64
}

func (c TodoAddressLatitudeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeNotEq struct {
	Longitude float64
}

func (c TodoAddressLongitudeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeNotEq struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeNotEq struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdGt struct {
	Id string
}

func (c TodoIdGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2Gt struct {
	I2 int32
}

func (c TodoI2Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3Gt struct {
	I3 int64
}

func (c TodoI3Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4Gt struct {
	I4 int32
}

func (c TodoI4Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5Gt struct {
	I5 int64
}

func (c TodoI5Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6Gt struct {
	I6 int32
}

func (c TodoI6Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7Gt struct {
	I7 int64
}

func (c TodoI7Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveGt struct {
	Active bool
}

func (c TodoActiveGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10Gt struct {
	I10 float64
}

func (c TodoI10Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11Gt struct {
	I11 float32
}

func (c TodoI11Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12Gt struct {
	I12 uint32
}

func (c TodoI12Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13Gt struct {
	I13 uint64
}

func (c TodoI13Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14Gt struct {
	I14 uint32
}

func (c TodoI14Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoGt struct {
	Todo []string
}

func (c TodoTodoGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeGt struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2Gt struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2Gt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageGt struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageGt struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdGt struct {
	Id string
}

func (c TodoToDoNotesIdGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyGt struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyGt struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountGt struct {
	Amount uint64
}

func (c TodoPriceAmountGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyGt struct {
	Currency string
}

func (c TodoPriceCurrencyGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatGt struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryGt struct {
	Country string
}

func (c TodoAddressCountryGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityGt struct {
	Locality string
}

func (c TodoAddressLocalityGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionGt struct {
	Region string
}

func (c TodoAddressRegionGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeGt struct {
	PostalCode string
}

func (c TodoAddressPostalCodeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressGt struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeGt struct {
	Latitude float64
}

func (c TodoAddressLatitudeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeGt struct {
	Longitude float64
}

func (c TodoAddressLongitudeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeGt struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeGt struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdLt struct {
	Id string
}

func (c TodoIdLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2Lt struct {
	I2 int32
}

func (c TodoI2Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3Lt struct {
	I3 int64
}

func (c TodoI3Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4Lt struct {
	I4 int32
}

func (c TodoI4Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5Lt struct {
	I5 int64
}

func (c TodoI5Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6Lt struct {
	I6 int32
}

func (c TodoI6Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7Lt struct {
	I7 int64
}

func (c TodoI7Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveLt struct {
	Active bool
}

func (c TodoActiveLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10Lt struct {
	I10 float64
}

func (c TodoI10Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11Lt struct {
	I11 float32
}

func (c TodoI11Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12Lt struct {
	I12 uint32
}

func (c TodoI12Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13Lt struct {
	I13 uint64
}

func (c TodoI13Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14Lt struct {
	I14 uint32
}

func (c TodoI14Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoLt struct {
	Todo []string
}

func (c TodoTodoLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeLt struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2Lt struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2Lt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageLt struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageLt struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdLt struct {
	Id string
}

func (c TodoToDoNotesIdLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyLt struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyLt struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountLt struct {
	Amount uint64
}

func (c TodoPriceAmountLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyLt struct {
	Currency string
}

func (c TodoPriceCurrencyLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatLt struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryLt struct {
	Country string
}

func (c TodoAddressCountryLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityLt struct {
	Locality string
}

func (c TodoAddressLocalityLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionLt struct {
	Region string
}

func (c TodoAddressRegionLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeLt struct {
	PostalCode string
}

func (c TodoAddressPostalCodeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressLt struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeLt struct {
	Latitude float64
}

func (c TodoAddressLatitudeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeLt struct {
	Longitude float64
}

func (c TodoAddressLongitudeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeLt struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeLt struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdGtOrEq struct {
	Id string
}

func (c TodoIdGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2GtOrEq struct {
	I2 int32
}

func (c TodoI2GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3GtOrEq struct {
	I3 int64
}

func (c TodoI3GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4GtOrEq struct {
	I4 int32
}

func (c TodoI4GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5GtOrEq struct {
	I5 int64
}

func (c TodoI5GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6GtOrEq struct {
	I6 int32
}

func (c TodoI6GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7GtOrEq struct {
	I7 int64
}

func (c TodoI7GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveGtOrEq struct {
	Active bool
}

func (c TodoActiveGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10GtOrEq struct {
	I10 float64
}

func (c TodoI10GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11GtOrEq struct {
	I11 float32
}

func (c TodoI11GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12GtOrEq struct {
	I12 uint32
}

func (c TodoI12GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13GtOrEq struct {
	I13 uint64
}

func (c TodoI13GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14GtOrEq struct {
	I14 uint32
}

func (c TodoI14GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoGtOrEq struct {
	Todo []string
}

func (c TodoTodoGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeGtOrEq struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2GtOrEq struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2GtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageGtOrEq struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageGtOrEq struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdGtOrEq struct {
	Id string
}

func (c TodoToDoNotesIdGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyGtOrEq struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyGtOrEq struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountGtOrEq struct {
	Amount uint64
}

func (c TodoPriceAmountGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyGtOrEq struct {
	Currency string
}

func (c TodoPriceCurrencyGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatGtOrEq struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryGtOrEq struct {
	Country string
}

func (c TodoAddressCountryGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityGtOrEq struct {
	Locality string
}

func (c TodoAddressLocalityGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionGtOrEq struct {
	Region string
}

func (c TodoAddressRegionGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeGtOrEq struct {
	PostalCode string
}

func (c TodoAddressPostalCodeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressGtOrEq struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeGtOrEq struct {
	Latitude float64
}

func (c TodoAddressLatitudeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeGtOrEq struct {
	Longitude float64
}

func (c TodoAddressLongitudeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeGtOrEq struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeGtOrEq struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdLtOrEq struct {
	Id string
}

func (c TodoIdLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2LtOrEq struct {
	I2 int32
}

func (c TodoI2LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3LtOrEq struct {
	I3 int64
}

func (c TodoI3LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4LtOrEq struct {
	I4 int32
}

func (c TodoI4LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5LtOrEq struct {
	I5 int64
}

func (c TodoI5LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6LtOrEq struct {
	I6 int32
}

func (c TodoI6LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7LtOrEq struct {
	I7 int64
}

func (c TodoI7LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveLtOrEq struct {
	Active bool
}

func (c TodoActiveLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10LtOrEq struct {
	I10 float64
}

func (c TodoI10LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11LtOrEq struct {
	I11 float32
}

func (c TodoI11LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12LtOrEq struct {
	I12 uint32
}

func (c TodoI12LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13LtOrEq struct {
	I13 uint64
}

func (c TodoI13LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14LtOrEq struct {
	I14 uint32
}

func (c TodoI14LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoLtOrEq struct {
	Todo []string
}

func (c TodoTodoLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeLtOrEq struct {
	NotificationType NotificationType
}

func (c TodoNotificationTypeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2LtOrEq struct {
	NotificationType2 NotificationType2
}

func (c TodoNotificationType2LtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageLtOrEq struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageLtOrEq struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdLtOrEq struct {
	Id string
}

func (c TodoToDoNotesIdLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyLtOrEq struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyLtOrEq struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountLtOrEq struct {
	Amount uint64
}

func (c TodoPriceAmountLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyLtOrEq struct {
	Currency string
}

func (c TodoPriceCurrencyLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatLtOrEq struct {
	AmountInFloat float32
}

func (c TodoPriceAmountInFloatLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryLtOrEq struct {
	Country string
}

func (c TodoAddressCountryLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityLtOrEq struct {
	Locality string
}

func (c TodoAddressLocalityLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionLtOrEq struct {
	Region string
}

func (c TodoAddressRegionLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeLtOrEq struct {
	PostalCode string
}

func (c TodoAddressPostalCodeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressLtOrEq struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeLtOrEq struct {
	Latitude float64
}

func (c TodoAddressLatitudeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeLtOrEq struct {
	Longitude float64
}

func (c TodoAddressLongitudeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeLtOrEq struct {
	StartTime *timestamp.Timestamp
}

func (c TodoDateslotStartTimeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeLtOrEq struct {
	EndTime *timestamp.Timestamp
}

func (c TodoDateslotEndTimeLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdLike struct {
	Id string
}

func (c TodoIdLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoLike struct {
	Todo []string
}

func (c TodoTodoLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageLike struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageLike struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdLike struct {
	Id string
}

func (c TodoToDoNotesIdLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyLike struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyLike struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyLike struct {
	Currency string
}

func (c TodoPriceCurrencyLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryLike struct {
	Country string
}

func (c TodoAddressCountryLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityLike struct {
	Locality string
}

func (c TodoAddressLocalityLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionLike struct {
	Region string
}

func (c TodoAddressRegionLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeLike struct {
	PostalCode string
}

func (c TodoAddressPostalCodeLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressLike struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdILike struct {
	Id string
}

func (c TodoIdILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoILike struct {
	Todo []string
}

func (c TodoTodoILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageILike struct {
	ThumbImage string
}

func (c TodoGalleryItemThumbImageILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageILike struct {
	LargeImage string
}

func (c TodoGalleryItemLargeImageILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdILike struct {
	Id string
}

func (c TodoToDoNotesIdILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyILike struct {
	HardCopy string
}

func (c TodoToDoNotesHardCopyILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyILike struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyILike struct {
	Currency string
}

func (c TodoPriceCurrencyILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryILike struct {
	Country string
}

func (c TodoAddressCountryILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityILike struct {
	Locality string
}

func (c TodoAddressLocalityILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionILike struct {
	Region string
}

func (c TodoAddressRegionILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeILike struct {
	PostalCode string
}

func (c TodoAddressPostalCodeILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressILike struct {
	StreetAddress string
}

func (c TodoAddressStreetAddressILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoTodoArrayOverlap struct {
	Todo []string
}

func (c TodoTodoArrayOverlap) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ArrayOverlap{Key: "todo", Value: c.Todo, Operator: d, Descriptor: &Todo{}, FieldMask: "todo", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDigitalCopyArrayOverlap struct {
	DigitalCopy []string
}

func (c TodoToDoNotesDigitalCopyArrayOverlap) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ArrayOverlap{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeleted struct {
	IsDeleted bool
}

func (c TodoDeleted) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesDeleted struct {
	IsDeleted bool
}

func (c TodoToDoNotesDeleted) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByEq struct {
	By string
}

func (c TodoCreatedByEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByNotEq struct {
	By string
}

func (c TodoCreatedByNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByGt struct {
	By string
}

func (c TodoCreatedByGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByLt struct {
	By string
}

func (c TodoCreatedByLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByGtOrEq struct {
	By string
}

func (c TodoCreatedByGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByLtOrEq struct {
	By string
}

func (c TodoCreatedByLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoCreatedOnLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByLike struct {
	By string
}

func (c TodoCreatedByLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoCreatedByILike struct {
	By string
}

func (c TodoCreatedByILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByEq struct {
	By string
}

func (c TodoUpdatedByEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByNotEq struct {
	By string
}

func (c TodoUpdatedByNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByGt struct {
	By string
}

func (c TodoUpdatedByGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByLt struct {
	By string
}

func (c TodoUpdatedByLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByGtOrEq struct {
	By string
}

func (c TodoUpdatedByGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByLtOrEq struct {
	By string
}

func (c TodoUpdatedByLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoUpdatedOnLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByLike struct {
	By string
}

func (c TodoUpdatedByLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoUpdatedByILike struct {
	By string
}

func (c TodoUpdatedByILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByEq struct {
	By string
}

func (c TodoDeletedByEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByNotEq struct {
	By string
}

func (c TodoDeletedByNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnNotEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByGt struct {
	By string
}

func (c TodoDeletedByGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnGt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByLt struct {
	By string
}

func (c TodoDeletedByLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnLt) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByGtOrEq struct {
	By string
}

func (c TodoDeletedByGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnGtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByLtOrEq struct {
	By string
}

func (c TodoDeletedByLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c TodoDeletedOnLtOrEq) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByLike struct {
	By string
}

func (c TodoDeletedByLike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDeletedByILike struct {
	By string
}

func (c TodoDeletedByILike) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Todo{}, RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdIn struct {
	Id []string
}

func (c TodoIdIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2In struct {
	I2 []int32
}

func (c TodoI2In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3In struct {
	I3 []int64
}

func (c TodoI3In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4In struct {
	I4 []int32
}

func (c TodoI4In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5In struct {
	I5 []int64
}

func (c TodoI5In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6In struct {
	I6 []int32
}

func (c TodoI6In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7In struct {
	I7 []int64
}

func (c TodoI7In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveIn struct {
	Active []bool
}

func (c TodoActiveIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10In struct {
	I10 []float64
}

func (c TodoI10In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11In struct {
	I11 []float32
}

func (c TodoI11In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12In struct {
	I12 []uint32
}

func (c TodoI12In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13In struct {
	I13 []uint64
}

func (c TodoI13In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14In struct {
	I14 []uint32
}

func (c TodoI14In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeIn struct {
	NotificationType []NotificationType
}

func (c TodoNotificationTypeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2In struct {
	NotificationType2 []NotificationType2
}

func (c TodoNotificationType2In) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageIn struct {
	ThumbImage []string
}

func (c TodoGalleryItemThumbImageIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageIn struct {
	LargeImage []string
}

func (c TodoGalleryItemLargeImageIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdIn struct {
	Id []string
}

func (c TodoToDoNotesIdIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyIn struct {
	HardCopy []string
}

func (c TodoToDoNotesHardCopyIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountIn struct {
	Amount []uint64
}

func (c TodoPriceAmountIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyIn struct {
	Currency []string
}

func (c TodoPriceCurrencyIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatIn struct {
	AmountInFloat []float32
}

func (c TodoPriceAmountInFloatIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryIn struct {
	Country []string
}

func (c TodoAddressCountryIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityIn struct {
	Locality []string
}

func (c TodoAddressLocalityIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionIn struct {
	Region []string
}

func (c TodoAddressRegionIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeIn struct {
	PostalCode []string
}

func (c TodoAddressPostalCodeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressIn struct {
	StreetAddress []string
}

func (c TodoAddressStreetAddressIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeIn struct {
	Latitude []float64
}

func (c TodoAddressLatitudeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeIn struct {
	Longitude []float64
}

func (c TodoAddressLongitudeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeIn struct {
	StartTime []*timestamp.Timestamp
}

func (c TodoDateslotStartTimeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeIn struct {
	EndTime []*timestamp.Timestamp
}

func (c TodoDateslotEndTimeIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoIdNotIn struct {
	Id []string
}

func (c TodoIdNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Todo{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI2NotIn struct {
	I2 []int32
}

func (c TodoI2NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_2", Value: c.I2, Operator: d, Descriptor: &Todo{}, FieldMask: "i_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI3NotIn struct {
	I3 []int64
}

func (c TodoI3NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_3", Value: c.I3, Operator: d, Descriptor: &Todo{}, FieldMask: "i_3", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI4NotIn struct {
	I4 []int32
}

func (c TodoI4NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_4", Value: c.I4, Operator: d, Descriptor: &Todo{}, FieldMask: "i_4", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI5NotIn struct {
	I5 []int64
}

func (c TodoI5NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_5", Value: c.I5, Operator: d, Descriptor: &Todo{}, FieldMask: "i_5", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI6NotIn struct {
	I6 []int32
}

func (c TodoI6NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_6", Value: c.I6, Operator: d, Descriptor: &Todo{}, FieldMask: "i_6", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI7NotIn struct {
	I7 []int64
}

func (c TodoI7NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_7", Value: c.I7, Operator: d, Descriptor: &Todo{}, FieldMask: "i_7", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoActiveNotIn struct {
	Active []bool
}

func (c TodoActiveNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "active", Value: c.Active, Operator: d, Descriptor: &Todo{}, FieldMask: "active", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI10NotIn struct {
	I10 []float64
}

func (c TodoI10NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_10", Value: c.I10, Operator: d, Descriptor: &Todo{}, FieldMask: "i_10", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI11NotIn struct {
	I11 []float32
}

func (c TodoI11NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_11", Value: c.I11, Operator: d, Descriptor: &Todo{}, FieldMask: "i_11", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI12NotIn struct {
	I12 []uint32
}

func (c TodoI12NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_12", Value: c.I12, Operator: d, Descriptor: &Todo{}, FieldMask: "i_12", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI13NotIn struct {
	I13 []uint64
}

func (c TodoI13NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_13", Value: c.I13, Operator: d, Descriptor: &Todo{}, FieldMask: "i_13", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoI14NotIn struct {
	I14 []uint32
}

func (c TodoI14NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "i_14", Value: c.I14, Operator: d, Descriptor: &Todo{}, FieldMask: "i_14", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationTypeNotIn struct {
	NotificationType []NotificationType
}

func (c TodoNotificationTypeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "notification_type", Value: c.NotificationType, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoNotificationType2NotIn struct {
	NotificationType2 []NotificationType2
}

func (c TodoNotificationType2NotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "notification_type_2", Value: c.NotificationType2, Operator: d, Descriptor: &Todo{}, FieldMask: "notification_type_2", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemThumbImageNotIn struct {
	ThumbImage []string
}

func (c TodoGalleryItemThumbImageNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "thumb_image", Value: c.ThumbImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_thumb_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoGalleryItemLargeImageNotIn struct {
	LargeImage []string
}

func (c TodoGalleryItemLargeImageNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "large_image", Value: c.LargeImage, Operator: d, Descriptor: &types.GalleryItem{}, FieldMask: "profile_image.profile_image_large_image", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesIdNotIn struct {
	Id []string
}

func (c TodoToDoNotesIdNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.id", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoToDoNotesHardCopyNotIn struct {
	HardCopy []string
}

func (c TodoToDoNotesHardCopyNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "todo_notes.hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountNotIn struct {
	Amount []uint64
}

func (c TodoPriceAmountNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "amount", Value: c.Amount, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceCurrencyNotIn struct {
	Currency []string
}

func (c TodoPriceCurrencyNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "currency", Value: c.Currency, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_currency", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoPriceAmountInFloatNotIn struct {
	AmountInFloat []float32
}

func (c TodoPriceAmountInFloatNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "amount_in_float", Value: c.AmountInFloat, Operator: d, Descriptor: &types.Price{}, FieldMask: "total_amount.total_amount_amount_in_float", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressCountryNotIn struct {
	Country []string
}

func (c TodoAddressCountryNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "country", Value: c.Country, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_country", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLocalityNotIn struct {
	Locality []string
}

func (c TodoAddressLocalityNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "locality", Value: c.Locality, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_locality", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressRegionNotIn struct {
	Region []string
}

func (c TodoAddressRegionNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "region", Value: c.Region, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_region", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressPostalCodeNotIn struct {
	PostalCode []string
}

func (c TodoAddressPostalCodeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "postal_code", Value: c.PostalCode, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_postal_code", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressStreetAddressNotIn struct {
	StreetAddress []string
}

func (c TodoAddressStreetAddressNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "street_address", Value: c.StreetAddress, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_street_address", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLatitudeNotIn struct {
	Latitude []float64
}

func (c TodoAddressLatitudeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "latitude", Value: c.Latitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_latitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoAddressLongitudeNotIn struct {
	Longitude []float64
}

func (c TodoAddressLongitudeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "longitude", Value: c.Longitude, Operator: d, Descriptor: &types.Address{}, FieldMask: "address.address_longitude", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotStartTimeNotIn struct {
	StartTime []*timestamp.Timestamp
}

func (c TodoDateslotStartTimeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "start_time", Value: c.StartTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_start_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

type TodoDateslotEndTimeNotIn struct {
	EndTime []*timestamp.Timestamp
}

func (c TodoDateslotEndTimeNotIn) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "end_time", Value: c.EndTime, Operator: d, Descriptor: &types.Dateslot{}, FieldMask: "date_range.date_range_end_time", RootDescriptor: &Todo{}, CurrentDescriptor: &Todo{}}
}

func (c TrueCondition) todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type todoMapperObject struct {
	id                string
	i2                int32
	i3                int64
	i4                int32
	i5                int64
	i6                int32
	i7                int64
	active            bool
	i10               float64
	i11               float32
	i12               uint32
	i13               uint64
	i14               uint32
	todo              []string
	notificationType  NotificationType
	notificationType2 NotificationType2
	profileImage      *types.GalleryItem
	todoNotes         map[string]*todoToDoNotesMapperObject
	totalAmount       *types.Price
	address           *types.Address
	dateRange         *types.Dateslot
}

func (s *todoMapperObject) GetUniqueIdentifier() string {
	return s.id
}

type todoToDoNotesMapperObject struct {
	id          string
	hardCopy    string
	digitalCopy []string
	details     []*any.Any
}

func (s *todoToDoNotesMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperTodo(rows []*Todo) []*Todo {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedTodoMappers := map[string]*todoMapperObject{}

	for _, rw := range rows {

		tempTodo := &todoMapperObject{}
		tempTodoToDoNotes := &todoToDoNotesMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		if len(rw.TodoNotes) == 0 {
			_ = rw.New("todo_notes")
			rw.TodoNotes = append(rw.TodoNotes, &ToDoNotes{})
			rw.TodoNotes[0] = rw.TodoNotes[0].GetEmptyObject()
		}
		tempTodoToDoNotes.id = rw.TodoNotes[0].Id
		tempTodoToDoNotes.hardCopy = rw.TodoNotes[0].HardCopy
		tempTodoToDoNotes.digitalCopy = rw.TodoNotes[0].DigitalCopy
		tempTodoToDoNotes.details = rw.TodoNotes[0].Details

		tempTodo.id = rw.Id
		tempTodo.i2 = rw.I2
		tempTodo.i3 = rw.I3
		tempTodo.i4 = rw.I4
		tempTodo.i5 = rw.I5
		tempTodo.i6 = rw.I6
		tempTodo.i7 = rw.I7
		tempTodo.active = rw.Active
		tempTodo.i10 = rw.I10
		tempTodo.i11 = rw.I11
		tempTodo.i12 = rw.I12
		tempTodo.i13 = rw.I13
		tempTodo.i14 = rw.I14
		tempTodo.todo = rw.Todo
		tempTodo.notificationType = rw.NotificationType
		tempTodo.notificationType2 = rw.NotificationType2
		tempTodo.profileImage = rw.ProfileImage
		tempTodo.todoNotes = map[string]*todoToDoNotesMapperObject{
			tempTodoToDoNotes.GetUniqueIdentifier(): tempTodoToDoNotes,
		}
		tempTodo.totalAmount = rw.TotalAmount
		tempTodo.address = rw.Address
		tempTodo.dateRange = rw.DateRange

		if combinedTodoMappers[tempTodo.GetUniqueIdentifier()] == nil {
			combinedTodoMappers[tempTodo.GetUniqueIdentifier()] = tempTodo
		} else {

			todoMapper := combinedTodoMappers[tempTodo.GetUniqueIdentifier()]

			if todoMapper.todoNotes[tempTodoToDoNotes.GetUniqueIdentifier()] == nil {
				todoMapper.todoNotes[tempTodoToDoNotes.GetUniqueIdentifier()] = tempTodoToDoNotes
			}
			combinedTodoMappers[tempTodo.GetUniqueIdentifier()] = todoMapper
		}

	}

	combinedTodos := make(map[string]*Todo, 0)

	for _, todo := range combinedTodoMappers {
		tempTodo := &Todo{}
		tempTodo.Id = todo.id
		tempTodo.I2 = todo.i2
		tempTodo.I3 = todo.i3
		tempTodo.I4 = todo.i4
		tempTodo.I5 = todo.i5
		tempTodo.I6 = todo.i6
		tempTodo.I7 = todo.i7
		tempTodo.Active = todo.active
		tempTodo.I10 = todo.i10
		tempTodo.I11 = todo.i11
		tempTodo.I12 = todo.i12
		tempTodo.I13 = todo.i13
		tempTodo.I14 = todo.i14
		tempTodo.Todo = todo.todo
		tempTodo.NotificationType = todo.notificationType
		tempTodo.NotificationType2 = todo.notificationType2
		tempTodo.ProfileImage = todo.profileImage

		combinedTodoToDoNotess := []*ToDoNotes{}

		for _, todoToDoNotes := range todo.todoNotes {
			tempTodoToDoNotes := &ToDoNotes{}
			tempTodoToDoNotes.Id = todoToDoNotes.id
			tempTodoToDoNotes.HardCopy = todoToDoNotes.hardCopy
			tempTodoToDoNotes.DigitalCopy = todoToDoNotes.digitalCopy
			tempTodoToDoNotes.Details = todoToDoNotes.details

			if tempTodoToDoNotes.Id == "" {
				continue
			}

			combinedTodoToDoNotess = append(combinedTodoToDoNotess, tempTodoToDoNotes)

		}
		tempTodo.TodoNotes = combinedTodoToDoNotess

		tempTodo.TotalAmount = todo.totalAmount
		tempTodo.Address = todo.address
		tempTodo.DateRange = todo.dateRange

		if tempTodo.Id == "" {
			continue
		}

		combinedTodos[tempTodo.Id] = tempTodo

	}
	list := make([]*Todo, 0, len(combinedTodos))
	for _, i := range ids {
		list = append(list, combinedTodos[i])
	}
	return list
}

func (s TodoStore) CreateToDoNotes(ctx context.Context, objFieldMask string, pid []string, list ...*ToDoNotes) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &ToDoNotes{}, &Todo{}, objFieldMask, pid)
	}
	return s.d.Insert(ctx, vv, &ToDoNotes{}, &Todo{}, objFieldMask, pid)
}

func (s TodoStore) DeleteToDoNotes(ctx context.Context, cond ToDoNotesCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{})
	}
	return s.d.Delete(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{})
}

func (s TodoStore) HardDeleteToDoNotes(ctx context.Context, cond ToDoNotesCondition) error {
	if s.withTx {
		return s.tx.HardDelete(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{})
	}
	return s.d.HardDelete(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{})
}

func (s TodoStore) UpdateToDoNotes(ctx context.Context, req *ToDoNotes, fields []string, cond ToDoNotesCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), req, &Todo{}, fields...)
	}
	return s.d.Update(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), req, &Todo{}, fields...)
}

func (s TodoStore) UpdateToDoNotesMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &ToDoNotes{}, &Todo{}, list...)
}

func (s TodoStore) GetToDoNotes(ctx context.Context, fields []string, cond ToDoNotesCondition, opt ...getToDoNotesTodoOption) (*ToDoNotes, error) {
	if len(fields) == 0 {
		fields = (&ToDoNotes{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listToDoNotesTodoOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListToDoNotes(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s TodoStore) ListToDoNotes(ctx context.Context, fields []string, cond ToDoNotesCondition, opt ...listToDoNotesTodoOption) ([]*ToDoNotes, error) {
	if len(fields) == 0 {
		fields = (&ToDoNotes{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetToDoNotesCondition == nil {
					page.SetToDoNotesCondition = defaultSetToDoNotesCondition
				}
				cond = page.SetToDoNotesCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*ToDoNotes, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &ToDoNotes{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperToDoNotes(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s TodoStore) CountToDoNotes(ctx context.Context, cond ToDoNotesCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.toDoNotesCondToDriverTodoCond(s.d), &ToDoNotes{}, &Todo{})
}

type getToDoNotesTodoOption interface {
	getOptToDoNotesTodo() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptToDoNotesTodo() { // method of no significant use
}

type listToDoNotesTodoOption interface {
	listOptToDoNotesTodo() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptToDoNotesTodo() {
}

func (OrderBy) listOptToDoNotesTodo() {
}

func (*CursorBasedPagination) listOptToDoNotesTodo() {
}

func defaultSetToDoNotesCondition(upOrDown bool, cursor string, cond ToDoNotesCondition) ToDoNotesCondition {
	if upOrDown {
		if cursor != "" {
			return ToDoNotesAnd{cond, ToDoNotesIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return ToDoNotesAnd{cond, ToDoNotesIdGt{cursor}}
	}
	return cond
}

type ToDoNotesAnd []ToDoNotesCondition

func (p ToDoNotesAnd) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.toDoNotesCondToDriverTodoCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type ToDoNotesOr []ToDoNotesCondition

func (p ToDoNotesOr) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.toDoNotesCondToDriverTodoCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type ToDoNotesParentEq struct {
	Parent string
}

func (c ToDoNotesParentEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesParentNotEq struct {
	Parent string
}

func (c ToDoNotesParentNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesParentLike struct {
	Parent string
}

func (c ToDoNotesParentLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesParentILike struct {
	Parent string
}

func (c ToDoNotesParentILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesParentIn struct {
	Parent []string
}

func (c ToDoNotesParentIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesParentNotIn struct {
	Parent []string
}

func (c ToDoNotesParentNotIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdEq struct {
	Id string
}

func (c ToDoNotesIdEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyEq struct {
	HardCopy string
}

func (c ToDoNotesHardCopyEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyEq struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdNotEq struct {
	Id string
}

func (c ToDoNotesIdNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyNotEq struct {
	HardCopy string
}

func (c ToDoNotesHardCopyNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyNotEq struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdGt struct {
	Id string
}

func (c ToDoNotesIdGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyGt struct {
	HardCopy string
}

func (c ToDoNotesHardCopyGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyGt struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdLt struct {
	Id string
}

func (c ToDoNotesIdLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyLt struct {
	HardCopy string
}

func (c ToDoNotesHardCopyLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyLt struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdGtOrEq struct {
	Id string
}

func (c ToDoNotesIdGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyGtOrEq struct {
	HardCopy string
}

func (c ToDoNotesHardCopyGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyGtOrEq struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdLtOrEq struct {
	Id string
}

func (c ToDoNotesIdLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyLtOrEq struct {
	HardCopy string
}

func (c ToDoNotesHardCopyLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyLtOrEq struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdLike struct {
	Id string
}

func (c ToDoNotesIdLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyLike struct {
	HardCopy string
}

func (c ToDoNotesHardCopyLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyLike struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdILike struct {
	Id string
}

func (c ToDoNotesIdILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyILike struct {
	HardCopy string
}

func (c ToDoNotesHardCopyILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyILike struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDigitalCopyArrayOverlap struct {
	DigitalCopy []string
}

func (c ToDoNotesDigitalCopyArrayOverlap) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ArrayOverlap{Key: "digital_copy", Value: c.DigitalCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "digital_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeleted struct {
	IsDeleted bool
}

func (c ToDoNotesDeleted) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByEq struct {
	By string
}

func (c ToDoNotesCreatedByEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByNotEq struct {
	By string
}

func (c ToDoNotesCreatedByNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByGt struct {
	By string
}

func (c ToDoNotesCreatedByGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByLt struct {
	By string
}

func (c ToDoNotesCreatedByLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByGtOrEq struct {
	By string
}

func (c ToDoNotesCreatedByGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByLtOrEq struct {
	By string
}

func (c ToDoNotesCreatedByLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesCreatedOnLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByLike struct {
	By string
}

func (c ToDoNotesCreatedByLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesCreatedByILike struct {
	By string
}

func (c ToDoNotesCreatedByILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByEq struct {
	By string
}

func (c ToDoNotesUpdatedByEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByNotEq struct {
	By string
}

func (c ToDoNotesUpdatedByNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByGt struct {
	By string
}

func (c ToDoNotesUpdatedByGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByLt struct {
	By string
}

func (c ToDoNotesUpdatedByLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByGtOrEq struct {
	By string
}

func (c ToDoNotesUpdatedByGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByLtOrEq struct {
	By string
}

func (c ToDoNotesUpdatedByLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesUpdatedOnLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByLike struct {
	By string
}

func (c ToDoNotesUpdatedByLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesUpdatedByILike struct {
	By string
}

func (c ToDoNotesUpdatedByILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByEq struct {
	By string
}

func (c ToDoNotesDeletedByEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByNotEq struct {
	By string
}

func (c ToDoNotesDeletedByNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnNotEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByGt struct {
	By string
}

func (c ToDoNotesDeletedByGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnGt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByLt struct {
	By string
}

func (c ToDoNotesDeletedByLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnLt) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByGtOrEq struct {
	By string
}

func (c ToDoNotesDeletedByGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnGtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByLtOrEq struct {
	By string
}

func (c ToDoNotesDeletedByLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ToDoNotesDeletedOnLtOrEq) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByLike struct {
	By string
}

func (c ToDoNotesDeletedByLike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesDeletedByILike struct {
	By string
}

func (c ToDoNotesDeletedByILike) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &ToDoNotes{}, RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdIn struct {
	Id []string
}

func (c ToDoNotesIdIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyIn struct {
	HardCopy []string
}

func (c ToDoNotesHardCopyIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesIdNotIn struct {
	Id []string
}

func (c ToDoNotesIdNotIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "id", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

type ToDoNotesHardCopyNotIn struct {
	HardCopy []string
}

func (c ToDoNotesHardCopyNotIn) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "hard_copy", Value: c.HardCopy, Operator: d, Descriptor: &ToDoNotes{}, FieldMask: "hard_copy", RootDescriptor: &Todo{}, CurrentDescriptor: &ToDoNotes{}}
}

func (c TrueCondition) toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type toDoNotesMapperObject struct {
	id          string
	hardCopy    string
	digitalCopy []string
	details     []*any.Any
}

func (s *toDoNotesMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperToDoNotes(rows []*ToDoNotes) []*ToDoNotes {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedToDoNotesMappers := map[string]*toDoNotesMapperObject{}

	for _, rw := range rows {

		tempToDoNotes := &toDoNotesMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempToDoNotes.id = rw.Id
		tempToDoNotes.hardCopy = rw.HardCopy
		tempToDoNotes.digitalCopy = rw.DigitalCopy
		tempToDoNotes.details = rw.Details

		if combinedToDoNotesMappers[tempToDoNotes.GetUniqueIdentifier()] == nil {
			combinedToDoNotesMappers[tempToDoNotes.GetUniqueIdentifier()] = tempToDoNotes
		}
	}

	combinedToDoNotess := make(map[string]*ToDoNotes, 0)

	for _, toDoNotes := range combinedToDoNotesMappers {
		tempToDoNotes := &ToDoNotes{}
		tempToDoNotes.Id = toDoNotes.id
		tempToDoNotes.HardCopy = toDoNotes.hardCopy
		tempToDoNotes.DigitalCopy = toDoNotes.digitalCopy
		tempToDoNotes.Details = toDoNotes.details

		if tempToDoNotes.Id == "" {
			continue
		}

		combinedToDoNotess[tempToDoNotes.Id] = tempToDoNotes

	}
	list := make([]*ToDoNotes, 0, len(combinedToDoNotess))
	for _, i := range ids {
		list = append(list, combinedToDoNotess[i])
	}
	return list
}

func (m *Todo) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type TodoCondition interface {
	todoCondToDriverTodoCond(d driver.Driver) driver.Conditioner
}
type ToDoNotesCondition interface {
	toDoNotesCondToDriverTodoCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetTodoCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetTodoCondition func(upOrDown bool, cursor string, cond TodoCondition) TodoCondition
	// SetToDoNotesCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetToDoNotesCondition func(upOrDown bool, cursor string, cond ToDoNotesCondition) ToDoNotesCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
