package pb

import (
	"context"

	. "github.com/golang/protobuf/ptypes/empty"
	"github.com/grpc-ecosystem/go-grpc-middleware/logging/zap/ctxzap"
	"go.saastack.io/protoc-gen-caw/convert"
	"go.uber.org/cadence/activity"
	"go.uber.org/cadence/workflow"
)

var (
	_ = Empty{}
	_ = convert.JsonB{}
)

const (
	TodosCreateTodoActivity     = "/saastack.v1.Todos/CreateTodo"
	TodosGetTodoActivity        = "/saastack.v1.Todos/GetTodo"
	TodosDeleteTodoActivity     = "/saastack.v1.Todos/DeleteTodo"
	TodosUpdateTodoActivity     = "/saastack.v1.Todos/UpdateTodo"
	TodosListTodoActivity       = "/saastack.v1.Todos/ListTodo"
	TodosBatchGetTodoActivity   = "/saastack.v1.Todos/BatchGetTodo"
	TodosListFilterToDoActivity = "/saastack.v1.Todos/ListFilterToDo"
)

func RegisterTodosActivities(cli TodosClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *CreateTodoRequest) (*Todo, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.CreateTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosCreateTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *GetTodoRequest) (*Todo, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.GetTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosGetTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *DeleteTodoRequest) (*Empty, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.DeleteTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosDeleteTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *UpdateTodoRequest) (*Todo, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.UpdateTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosUpdateTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListTodoRequest) (*ListTodoResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosListTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchGetTodoRequest) (*BatchGetTodoResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.BatchGetTodo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosBatchGetTodoActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ListFilterToDoRequest) (*ListTodoResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ListFilterToDo(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: TodosListFilterToDoActivity},
	)
}

// TodosActivitiesClient is a typesafe wrapper for TodosActivities.
type TodosActivitiesClient struct {
}

// NewTodosActivitiesClient creates a new TodosActivitiesClient.
func NewTodosActivitiesClient(cli TodosClient) TodosActivitiesClient {
	RegisterTodosActivities(cli)
	return TodosActivitiesClient{}
}

func (ca *TodosActivitiesClient) CreateTodo(ctx workflow.Context, in *CreateTodoRequest) (*Todo, error) {
	future := workflow.ExecuteActivity(ctx, TodosCreateTodoActivity, in)
	var result Todo
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) GetTodo(ctx workflow.Context, in *GetTodoRequest) (*Todo, error) {
	future := workflow.ExecuteActivity(ctx, TodosGetTodoActivity, in)
	var result Todo
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) DeleteTodo(ctx workflow.Context, in *DeleteTodoRequest) (*Empty, error) {
	future := workflow.ExecuteActivity(ctx, TodosDeleteTodoActivity, in)
	var result Empty
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) UpdateTodo(ctx workflow.Context, in *UpdateTodoRequest) (*Todo, error) {
	future := workflow.ExecuteActivity(ctx, TodosUpdateTodoActivity, in)
	var result Todo
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) ListTodo(ctx workflow.Context, in *ListTodoRequest) (*ListTodoResponse, error) {
	future := workflow.ExecuteActivity(ctx, TodosListTodoActivity, in)
	var result ListTodoResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) BatchGetTodo(ctx workflow.Context, in *BatchGetTodoRequest) (*BatchGetTodoResponse, error) {
	future := workflow.ExecuteActivity(ctx, TodosBatchGetTodoActivity, in)
	var result BatchGetTodoResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *TodosActivitiesClient) ListFilterToDo(ctx workflow.Context, in *ListFilterToDoRequest) (*ListTodoResponse, error) {
	future := workflow.ExecuteActivity(ctx, TodosListFilterToDoActivity, in)
	var result ListTodoResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

const (
	ParentServiceValidateParentActivity      = "/saastack.v1.ParentService/ValidateParent"
	ParentServiceBatchValidateParentActivity = "/saastack.v1.ParentService/BatchValidateParent"
)

func RegisterParentServiceActivities(cli ParentServiceClient) {
	activity.RegisterWithOptions(
		func(ctx context.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.ValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceValidateParentActivity},
	)
	activity.RegisterWithOptions(
		func(ctx context.Context, in *BatchValidateParentRequest) (*BatchValidateParentResponse, error) {
			ctx = ctxzap.ToContext(ctx, activity.GetLogger(ctx))
			res, err := cli.BatchValidateParent(ctx, in)
			return res, err
		},
		activity.RegisterOptions{Name: ParentServiceBatchValidateParentActivity},
	)
}

// ParentServiceActivitiesClient is a typesafe wrapper for ParentServiceActivities.
type ParentServiceActivitiesClient struct {
}

// NewParentServiceActivitiesClient creates a new ParentServiceActivitiesClient.
func NewParentServiceActivitiesClient(cli ParentServiceClient) ParentServiceActivitiesClient {
	RegisterParentServiceActivities(cli)
	return ParentServiceActivitiesClient{}
}

func (ca *ParentServiceActivitiesClient) ValidateParent(ctx workflow.Context, in *ValidateParentRequest) (*ValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceValidateParentActivity, in)
	var result ValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

func (ca *ParentServiceActivitiesClient) BatchValidateParent(ctx workflow.Context, in *BatchValidateParentRequest) (*BatchValidateParentResponse, error) {
	future := workflow.ExecuteActivity(ctx, ParentServiceBatchValidateParentActivity, in)
	var result BatchValidateParentResponse
	if err := future.Get(ctx, &result); err != nil {
		return nil, err
	}
	return &result, nil
}
