package todo

import (
	"context"

	"go.saastack.io/todo/pb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type todosServer struct {
	todoStore pb.TodoStore
	todoBLoC  pb.TodosServiceTodoServerBLoC
	*pb.TodosServiceTodoServerCrud
}

func NewTodosServer(

	todoSt pb.TodoStore,

) pb.TodosServer {
	r := &todosServer{

		todoStore: todoSt,
	}

	todoSC := pb.NewTodosServiceTodoServerCrud(todoSt, r)
	r.TodosServiceTodoServerCrud = todoSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *todosServer) GetTodoBLoC(ctx context.Context, in *pb.GetTodoRequest) error {
	return nil
}

func (s *todosServer) UpdateTodoBLoC(ctx context.Context, in *pb.UpdateTodoRequest) error {
	return nil
}

func (s *todosServer) DeleteTodoBLoC(ctx context.Context, in *pb.DeleteTodoRequest) error {
	return nil
}

func (s *todosServer) BatchGetTodoBLoC(ctx context.Context, in *pb.BatchGetTodoRequest) error {
	return nil
}

func (s *todosServer) ListTodoBLoC(ctx context.Context, in *pb.ListTodoRequest) (pb.TodoCondition, error) {
	panic("implement me")
}

// These functions are not implemented by CRUDGen, needed to be implemented

func (s *todosServer) ListFilterToDo(ctx context.Context, in *pb.ListFilterToDoRequest) (*pb.ListTodoResponse, error) {
	panic("implement me")
}

func (s *todosServer) CreateTodo(ctx context.Context, in *pb.CreateTodoRequest) (*pb.Todo, error) {
	panic("implement me")
}

type parentServiceServer struct {

}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer() pb.ParentServiceServer {
	return &parentServiceServer{}
}

//ValidateParent ...
func (s parentServiceServer) ValidateParent(ctx context.Context, request *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
	panic("implement me")
}

func (s parentServiceServer) BatchValidateParent(ctx context.Context, request *pb.BatchValidateParentRequest) (*pb.BatchValidateParentResponse, error) {
	panic("implement me")
}