package todo

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"go.saastack.io/jaal/schemabuilder"
	"go.saastack.io/todo/pb"
	"go.uber.org/fx"
	"google.golang.org/grpc"
)

//Module is the fx module encapsulating all the providers of the package
var Module = fx.Options(
	fx.Provide(

		pb.NewPostgresTodoStore,

		NewTodosServer,
		fx.Annotated{
			Name: "public",
			Target: func(srv pb.TodosServer) pb.TodosServer {
				return srv
			},
		},

		pb.NewLocalTodosClient,
		fx.Annotated{
			Name: "public",
			Target: func(in struct {
				fx.In
				S pb.TodosServer `name:"public"`
			}) pb.TodosClient {
				return pb.NewLocalTodosClient(in.S)
			},
		},

		fx.Annotated{
			Group:  "grpc-service",
			Target: RegisterGRPCService,
		},
		fx.Annotated{
			Group:  "graphql-service",
			Target: RegisterGraphQLService,
		},
		fx.Annotated{
			Group:  "http-service",
			Target: RegisterHttpService,
		},

		NewParentServiceServer,
		pb.NewLocalParentServiceClient,
	),
	pb.UnregisteredMethods_Todos,
	fx.Decorate(
		NewLogsTodosServer,
		pb.NewEventsTodosServer,
		pb.NewRightsTodosServer,
		pb.NewTraceTodosServer,
	),
)

func RegisterGRPCService(in struct {
	fx.In
	Server pb.TodosServer `name:"public"`
}) func(s *grpc.Server) {
	return func(s *grpc.Server) {
		pb.RegisterTodosServer(s, in.Server)
	}
}

func RegisterGraphQLService(in struct {
	fx.In
	Client pb.TodosClient `name:"public"`
}) func(s *schemabuilder.Schema) {
	return func(s *schemabuilder.Schema) {
		pb.RegisterTodosOperations(s, in.Client)
	}
}

func RegisterHttpService(in struct {
	fx.In
	Client pb.TodosClient `name:"public"`
}) func(*runtime.ServeMux, context.Context) error {
	return func(mux *runtime.ServeMux, ctx context.Context) error {
		return pb.RegisterTodosHandlerClient(ctx, mux, in.Client)
	}
}
