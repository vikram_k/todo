module go.saastack.io/todo

go 1.15

replace (
	github.com/apache/thrift => github.com/apache/thrift v0.0.0-20190309152529-a9b748bb0e02
	//go.saastack.io/chaku => go.saastack.io/chaku v0.1.0
	go.uber.org/dig => github.com/paullen/dig v1.7.1-0.20190624104937-6e47ebbbdcf6
	go.uber.org/fx => github.com/appointy/fx v1.9.1-0.20190624110333-490d04d33ef6
	google.golang.org/grpc => google.golang.org/grpc v1.29.1
)

require (
	github.com/Shivam010/protoc-gen-validate v0.3.0
	github.com/elgris/sqrl v0.0.0-20190909141434-5a439265eeec
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	go.opencensus.io v0.23.0
	go.saastack.io/activity-log v0.0.0-20210608115129-12c7b7f1c168
	go.saastack.io/chaku v0.2.0
	go.saastack.io/deployment/events v0.0.0-20210626065756-e7b4437bce20
	go.saastack.io/deployment/right v0.0.0-20210626065756-e7b4437bce20
	go.saastack.io/events v0.0.0-20200615072044-33e4fefd1624
	go.saastack.io/eventspush v0.0.0-20210506072653-47410fe208cd
	go.saastack.io/idutil v0.0.0-20200122122527-bf060a213abc
	go.saastack.io/jaal v0.0.0-20210129082513-56f41dab727f
	go.saastack.io/modulerole v0.0.0-20210615102646-a4ed74cb983c
	go.saastack.io/pehredaar v0.0.0-20200810065542-af2466b1ff34
	go.saastack.io/pii v0.0.0-20210420123411-b80bc9b0ec8b
	go.saastack.io/protoc-gen-caw/convert v0.0.0-20210206112258-37a2c0aacda4
	go.saastack.io/protoc-gen-grpc-wrapper v0.0.0-20201124092054-65b8a2efbc5c
	go.saastack.io/protoc-gen-log v0.0.0-20210607080548-535b08409bc9
	go.saastack.io/protoc-gen-nakaab v0.0.0-20200124074048-9eedb7860ee1
	go.saastack.io/protos v0.0.0-20210531072045-bf4ac753192f
	go.saastack.io/right v0.0.0-20210611055322-409da4e55df6
	go.saastack.io/userinfo v0.0.0-20200522114021-e48273ccb360
	go.uber.org/cadence v0.17.0
	go.uber.org/fx v1.12.0
	go.uber.org/zap v1.17.0
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e
	google.golang.org/genproto v0.0.0-20210624195500-8bfb893ecb84
	google.golang.org/grpc v1.38.0
)
